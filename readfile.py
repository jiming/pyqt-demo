import requests
from bs4 import BeautifulSoup

header = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Connection': 'keep-alive',
    'Cookie': 'Hm_lvt_492109f03bd65de28452325006c4a53c=1542358828,1542359067,1542359110; Hm_lpvt_492109f03bd65de28452325006c4a53c=1542359124',
    'Host': 'www.win4000.com',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
}

imgheader = {
    'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Connection': 'keep-alive',
    'Cookie': 'Hm_lvt_492109f03bd65de28452325006c4a53c=1542359067,1542359110,1542359346,1542359419; Hm_lpvt_492109f03bd65de28452325006c4a53c=1542359437',
    'Host': 'pic1.win4000.com',
    'Referer': 'http://www.win4000.com/meitu.html',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
}
resp = requests.get('http://mm.qingqujie.com/tag/dadanrenti2.html')
resp.encoding = 'utf-8'
soup = BeautifulSoup(resp.text, 'html.parser')
imgs = soup.find_all('img')
for img in imgs:
    if 'src' in img.attrs:
        src = img['src'].split('?')[0]
        if 'http' in src:
            print(src)
            filename = src.split('/')[-1]
            resp = requests.get(src)
            with open('D:/pyimages/%s' % filename, 'wb') as f:
                f.write(resp.content)
