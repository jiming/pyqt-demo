# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'InfoWin.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.listView = QtWidgets.QListView(Form)
        self.listView.setObjectName("listView")
        self.gridLayout.addWidget(self.listView, 0, 0, 1, 2)
        self.prevPageButton = QtWidgets.QPushButton(Form)
        self.prevPageButton.setObjectName("prevPageButton")
        self.gridLayout.addWidget(self.prevPageButton, 1, 0, 1, 1)
        self.nextPageButton = QtWidgets.QPushButton(Form)
        self.nextPageButton.setObjectName("nextPageButton")
        self.gridLayout.addWidget(self.nextPageButton, 1, 1, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "info"))
        self.prevPageButton.setText(_translate("Form", "上一页"))
        self.nextPageButton.setText(_translate("Form", "下一页"))

