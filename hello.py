# !/usr/bin/python3
import hashlib
import threading


import time

import os
import pymysql


class Student:
    def __init__(self, name):
        self.name = name


def hello(**kwargs):
    print(kwargs['name'])
    print(kwargs['age'])


def tests(*args):
    print(args)
    print(args[0])
    print(args[1])
    print(args[2])


def loop(num):
    print('the start time is %s'%time.ctime())
    time.sleep(num)
    print('the end time is %s'%time.ctime())

class Mythread(threading.Thread):
    def __init__(self):
        super().__init__()

    def run(self):
        #这里是将threading.Thread中的run方法进行了重载
        # self.func(self.args)
        print(threading.current_thread().name)

        pass

def main():
    print(threading.current_thread().name)
    t=Mythread()
    f=Mythread()

    t.start()
    #解释：这里的start可以说是调用run的方法
    f.start()

    t.join()
    f.join()





if __name__ == '__main__':
    print(os.path.splitext('https://www.cnblogs.com/yongqing/p/7067834.html')[1])
    # main()
    # md5 = hashlib.md5()
    # md5.update('how to use md5 in '.encode("utf-8"))
    # md5.update('python hashlib?'.encode("utf-8"))
    # print(md5.hexdigest())
    # print(os.path.splitext('https://www.cnblogs.com/yongqing/p/7067834.html'))
    # tests('str', 0, Student('hello'))
    # hello(name = 'lily', age = 19)
    # hello('lily', time='2015-01-01 00:00:00', age=15)
    # str = 'hello,world!'
    # nums = list(range(10))
    # print(nums[::-1])
    # for i in range(len(str)):
    #     print(str[i])
    #
    #
    #
    # # 打开数据库连接
    # db = pymysql.connect("localhost", "root", "123abc", "qhfax")
    #
    # # 使用 cursor() 方法创建一个游标对象 cursor
    # cursor = db.cursor()
    #
    # # 使用 execute()  方法执行 SQL 查询
    # cursor.execute("SELECT * FROM t_user")
    #
    # # 使用 fetchone() 方法获取单条数据.
    # data = cursor.fetchone()
    #
    # print("Database version : %s " % data)
    #
    # # 关闭数据库连接
    # db.close()

