import sys

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QStandardItemModel, QPixmap, QStandardItem
from PyQt5.QtWidgets import QApplication, QLabel
from bs4 import BeautifulSoup
import requests
from App import InfoTableView


class MovieInfoTableView(InfoTableView):

    def __init__(self, pageSize = 10, pageNo = 1):
        super().__init__(pageSize, pageNo)
        self.setWindowTitle("豆瓣电影 Top 250")
        self.model = QStandardItemModel(self.pageSize, 5)
        self.model.setHorizontalHeaderLabels(["电影名", "简述", "导演/主演", "评分", "海报"])
        self.tableView.setModel(self.model)

    def loadPage(self):

        header = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Cookie': 'bid=_iW8qDP3GPM; douban-fav-remind=1; ll="118282"; _vwo_uuid_v2=DB8FD3FCC5CA6243E599D0B1D0EFD0C05|5504c96ccc06dd025ab156f5506ea5cd; __utmz=30149280.1541400040.6.6.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; __utma=30149280.921503327.1540537994.1541400040.1542336628.7; __utmc=30149280; __utmb=30149280.1.10.1542336628; ap_v=0,6.0; __utma=223695111.22837598.1540806377.1540806377.1542336629.2; __utmb=223695111.0.10.1542336629; __utmc=223695111; __utmz=223695111.1542336629.2.2.utmcsr=douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/; _pk_ref.100001.4cf6=%5B%22%22%2C%22%22%2C1542336629%2C%22https%3A%2F%2Fwww.douban.com%2F%22%5D; _pk_ses.100001.4cf6=*; _pk_id.100001.4cf6=3acab5c9668e4b63.1540806376.2.1542337815.1540806376.',
            'Host': 'movie.douban.com',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
        }
        resp = requests.get('https://movie.douban.com/top250?start=%d&filter=' % (self.pageSize * (self.pageNo - 1)), header)
        resp.encoding = 'utf-8'
        soup = BeautifulSoup(resp.text, 'html.parser')
        movies = soup.find('ol', class_='grid_view').find_all('li')
        print(movies)

        row = 0
        for m in movies:
            pic = m.find('div', class_='pic')
            infohd = m.find('div', class_='hd')
            infobd = m.find('div', class_='bd')
            item = QStandardItem(infohd.a.find('span').text)
            self.model.setItem(row, 0, item)
            item = QStandardItem(infobd.find('span', class_='inq').text)
            self.model.setItem(row, 1, item)
            item = QStandardItem(infobd.find('p').text)
            self.model.setItem(row, 2, item)
            item = QStandardItem(infobd.find('span', class_='rating_num').text)
            self.model.setItem(row, 3, item)

            resp = requests.get(pic.a.img['src'])
            img = QPixmap()
            img.loadFromData(resp.content)
            label = QLabel()
            label.setPixmap(img);
            item = QStandardItem(infobd.find('p').text)
            item.setSizeHint(QSize(img.width(), img.height()))
            index = self.model.index(row, 4)
            self.tableView.setIndexWidget(index, label)
            row += 1

        self.tableView.resizeRowsToContents()
        self.tableView.resizeColumnsToContents()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = MovieInfoTableView(25, 1)
    win.loadPage()
    win.show()
    sys.exit(app.exec_())
